# **Apis Comunes**

-	/findAll
-	/findById
-	/modify
-	/delete/{id}
	
### AutorController

-	/findByApe{apellido}
-	/findByApeyNom/{apellido, nombre, dni}
	
### EditorialController	

-	/findEditorial/{nombre}
	
### EjemplarController

-	/findDisp/{idLibro}	
	
### GeneroController

-	/findByName/{nombre}
	
### LibroController

### PrestamoController		

### SocioController

-	/findByApe/{apellido}
-	/findByApeyNom
	
### TemaController

-	/findByName/{nombre}
	