Comandos básicos

# Más usados
docker run
docker ps
docker build
docker images
docker logs
docker inspect
docker volume

# Otros comandos comunes
docker commit
docker pull
docker push
docker tag

Nuestro primer contenedor

$ docker run --rm -it alpine

Dockerfile
// archivo de texto plano para crear imagenes docker

FROM alpine:3.8
# Instalar Nginx y configurar una página personalizada
RUN mkdir -p /run/nginx /www
RUN echo "<html><h1>Nginx en Docker</h1></html>" > /www/index.html
RUN apk add -U nginx
RUN echo 'server { listen 80; root /www; }' > /etc/nginx/conf.d/default.conf
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]