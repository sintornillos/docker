// Extensión REST CLIENT 

// ---------------- Consultas socio
@URL = http://localhost:8000/

// Lista de todos los socios
###
GET http://localhost:8000/socio/findAll

// Alta de un socio
###
POST http://localhost:8000/socio
Content-Type: application/json

{
  "apellido": "Baza",
  "nombre": "Marita",
  "nro_docum": "22112211",
  "direcc": "31 nro 1102 depto 2",
  "barrio": " ",
  "ciudad": "Dolores",
  "cpostal": "7100",
  "telefono": "0221494946",
  "celular": " ",
  "email": "marbaza@hotmail.com",
  "sexo":"F",
  "categoria": "8"
}

// Alta de un socio con nro de socio
###
POST http://localhost:8000/socio
Content-Type: application/json

{
    "apellido": "Fabrega",
    "nombre": "Horacio",
    "nroSocio": "23",
    "nroDocumento": "343434348",
    "direcc": "5 nro 102 depto 5",
    "barrio": " ",
    "ciudad": "San Miguel",
    "telefono": "0221494946",
    "celular": " ",
    "email": "mlaza@hotmail.com",
    "cpostal": 7100,
    "sexo": "M",
    "categoria": "5"
}

###
POST http://localhost:8000/socio
Content-Type: application/json

{
    "apellido": "Lopez",
    "nombre": "Luis",
    "nroSocio": "34",
    "nroDocumento": "1234567",
    "direcc": "31 nro 134 depto 5",
    "barrio": "Los Troncos",
    "ciudad": "Gral Belgrano",
    "telefono": "0221494646",
    "celular": null,
    "email": "lopez@gmail.com",
    "cpostal": 1900,
    "fecAlta": null,
    "sexo": "M",
    "categoria": "8"
}

// Busqueda por Id
###
GET http://localhost:8000/socio/findById/1

// Modificación del socio
###
PUT http://localhost:8000/socio
Content-Type: application/json

{
    "id": 1,
    "apellido": "Gomez",
    "nombre": "Graciela",
    "nroDocumento": "32132132",
    "direcc": "31 nro 112 depto 2",
    "barrio": "Los Troncos",
    "ciudad": "Gral Belgrano",
    "telefono": "0221494646",
    "celular": null,
    "email": "marbaza@gmail.com",
    "cpostal": 1900,
    "fecAlta": null,
    "sexo": "M",
    "categoria": "8"
}

// Consulta por apellido
###
GET http://localhost:8000/socio/findByApe/Gomez

// Consulta por parte del apellido
###
GET http://localhost:8000/socio/findByApe/ez

###
GET {{URL}}/socio/findByApeyNom?ape=ez&doc=32132132