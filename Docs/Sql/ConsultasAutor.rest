// Extensión REST CLIENT 
// de Huachao Mao
// Este es una manera de probar las consultas mediante 
// un plugin de visual studio code llamado 
// REST CLIENT de Huachao Mao, se ejecuta cada consulta
// presionando control y haciendo click sobre la consulta

// Consultas a la tabla de Autor
@URL = http://localhost:8000

###
 GET http://localhost:8000/autor/findAll

###
POST http://localhost:8000/autor HTTP/1.1
Content-Type: application/json

{
  "nombre": "Lama",
  "apellido": "Capo",
  "nacionalidad": "Arg"
}

###
POST http://localhost:8000/autor HTTP/1.1
Content-Type: application/json

{
  "nombre": "Pablo",
  "apellido": "Neruda",
  "nacionalidad": "Chi"
}

###
POST http://localhost:8000/autor HTTP/1.1
Content-Type: application/json

{ 
  "apellido": "Borges", 
  "nombre": "Jorge",
  "nacionalidad": "Arg" 
}  

// Busqueda de Autor por Apellido o nombre o Id
###
GET http://localhost:8000/autor/findById/2

// Busqueda por Apellido
###
GET {{URL}}/autor/findByApe/Capo

// Busqueda por parte del apellido
###
GET {{URL}}/autor/findByApeyNom?ape=po

// Busqueda por parte del nombre
###
GET {{URL}}/autor/findByApeyNom?nom=ma