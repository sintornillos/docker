# Server App Biblioteca

EL servidor esta desarrollado en Java, con Maven y Spring Tools 4

La Base de datos esta basada en Mysql Community Edition


1. Clonar desde el repositorio 
    `http://gitlab.com/sintornillos/serverbiblio.git`

2. Dentro de la carpeta /main/resources se encuentra el archivo application.properties donde se define la conexión a la base de datos.

* En este archivo :

* Se define el puerto, nosotros en este caso estamos usando el 8000

* Se define el nombre de la base, (para desarrollo se uso dbdesa)

* El usuario y la password de conexión a la base de datos

* Despúes de la primera ejecución se debe cambiar *"create"* en la línea *
`spring.jpa.hibernate.ddl-auto=create`

    por *"none"* , esto evita que me borre todos los datos de la base.


3. En el menu de Eclipse, opción Run, Run As...
elegir Spring Boot App

Y con esto deberíamos tener el servidor funcionando...


