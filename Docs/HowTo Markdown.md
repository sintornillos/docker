# Markdown

Si empiezo a escrbir es un parrafo, si doy (Enter)
se produce un salto de linea.
Para comenzar un nuevo parrafo hay que dejar una linea en blanco

Aqui comienzo un nuevo parrafo.-

Para crear un encabezado coloco una almohadilla (#),
cada una que coloco incremente un nivel del encabezado,
puedo colocar hasta 6 niveles. (Solo se admite un nivel 1)

## Este es un Encabezado (nivel 2)

### Este es otro (nivel 3)

Para incluir citas textuales se usa el simbolo (>) seguido de un espacio

> este es un comentario textual

Para hacer listas podemos usar el simbolo (\*) o (-)

- item 1
- item 2
- item 3
- - item 3.1
- - item 3.2

Para hacer listas ordenadas numericamente, se coloca el número seguido de un punto y un espacio

1. Producto 1
2. Produc 2
3. Product 3

Tambien podemos hacer listas anidadas incluyendo un espacio y un tab

- Lunes
  1. Mañana
  2. Tarde
  3. Noche
- Martes
  1. Mañana
  2. Tarde

Para escribir en Negrita se encierra con dos asteriscos
**Negrita**

La Cursiva se encierra con un solo asterisco _Cursiva_
Para el texto Tachado se encierra con el simbolo (~) ~~Tachado~~

y todas las combinaciones de las anteriores.-

para subrayar una palabra los haremos del siguiente modo:
<u>aprender markdown</u>

Para hacer listas de tareas (check list) se coloca un doble corchete con un espacio

hay que hacer esto [ ]
y tambien esto [ ]
esto ya lo termine [x]

ahora lo voy ha hacer como una lista de tareas

## Check List

### Lista de Tareas

- hay que hacer esto [ ]
- y tambien esto [ ]
- esto ya lo termine [x]

la barra invertida hace que ignore el markdown \* como en
la linea \*
