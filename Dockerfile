#
# Build Stage
#
FROM maven:3.6.1-jdk-8 as build 
COPY . /home/app
WORKDIR /home/app
RUN mvn  clean package

#
# Package Stage
#
#FROM openjdk:8.0.51-jre8-alpine
FROM openjdk:8-jre-alpine
COPY --from=build /home/app/target/serverBiblio-0.0.1-SNAPSHOT.war /usr/local/lib/serverbiblio.war
EXPOSE 8000
CMD [ "sh", "-c", "java -Dspring.profiles.active=docker -Djava.security.egd=file:/dev/./urandom -jar /usr/local/lib/serverbiblio.war" ]
