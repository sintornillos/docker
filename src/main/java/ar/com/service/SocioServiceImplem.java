package ar.com.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.com.dto.SocioDTO;
import ar.com.model.Socio;
import ar.com.repository.SocioRepository;

@Service
public class SocioServiceImplem implements SocioService {
	
	//Atributos
	@Autowired
	private SocioRepository socioRepository;
	
	public SocioServiceImplem() {
		
	}
	
	//Metodos
	@Override
	public SocioDTO create(SocioDTO socioDTO) {
		Socio pers = new Socio(
				socioDTO.getApellido(),
				socioDTO.getNombre(),
				socioDTO.getNroSocio(),
				socioDTO.getNroDocumento(),
				socioDTO.getDirecc(),
				socioDTO.getBarrio(),
				socioDTO.getCiudad(),
				socioDTO.getTelefono(),
				socioDTO.getCelular(),
				socioDTO.getEmail(),
				socioDTO.getCpostal(),
				socioDTO.getSexo(),
				socioDTO.getCategoria()
				);
		pers.setFecAlta(LocalDate.now());
		this.socioRepository.save(pers);
		return new SocioDTO(pers);
	}

	@Override
	public SocioDTO findById(Long idSocio) {
		Socio socio = this.socioRepository.findById(idSocio).orElse(null);
		SocioDTO socioDTO = null;
		if(socio != null) {
			socioDTO = new SocioDTO(socio);
		}
		return socioDTO;
	}

	@Override
	public List<SocioDTO> findAll() {
		List<Socio> socios = (List<Socio>) this.socioRepository.findAll();
		List<SocioDTO> sociosDTO = new ArrayList<SocioDTO>();
		socios.stream().forEach(per -> sociosDTO.add(new SocioDTO(per)));
		return sociosDTO;
	}
	
	@Override
	public List<SocioDTO> buscarApellido(String ape) {
		List<Socio> socios = (List<Socio>) this.socioRepository.buscarApellido(ape);
		List<SocioDTO> sociosDTO = new ArrayList<SocioDTO>();
		socios.stream().forEach(per -> sociosDTO.add(new SocioDTO(per)));
		return sociosDTO;
	}
	
	@Override
	public List<SocioDTO> buscarApeyNom(String apellido, String nombre, Long nroDocumento) {
		List<Socio> socios = (List<Socio>) this.socioRepository.buscarApellidoNombre(apellido, nombre, nroDocumento);
		List<SocioDTO> sociosDTO = new ArrayList<SocioDTO>();
		socios.stream().forEach(per -> sociosDTO.add(new SocioDTO(per)));
		return sociosDTO;
	}
	
	@Override
	public void deleteById(Long idSocio) {
		this.socioRepository.deleteById(idSocio);
	}
	
	@Override
	public SocioDTO update(SocioDTO socioDTO) {
		Socio socio = this.socioRepository.findById(socioDTO.getId()).get();
			socio.setApellido(socioDTO.getApellido());
			socio.setNombre(socioDTO.getNombre());
			socio.setNroSocio(socioDTO.getNroSocio());
			socio.setNroDocumento(socioDTO.getNroDocumento());
			socio.setDirecc(socioDTO.getDirecc());
			socio.setBarrio(socioDTO.getBarrio());
			socio.setCiudad(socioDTO.getCiudad());
			socio.setCpostal(socioDTO.getCpostal());
			socio.setTelefono(socioDTO.getTelefono());
			socio.setEmail(socioDTO.getEmail());
			socio.setSexo(socioDTO.getSexo());
			socio.setCategoria(socioDTO.getCategoria());
			socio = this.socioRepository.save(socio);
			return socioDTO;
	}

	
}
