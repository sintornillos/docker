package ar.com.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.com.dto.GeneroDTO;
import ar.com.model.Genero;
import ar.com.repository.GeneroRepository;

/**
 * @author Carlos
 *
 */
@Service
public class GeneroServiceImplem implements GeneroService {
	
	//Atributos
	@Autowired
	private GeneroRepository generoRepository;
	
	public GeneroServiceImplem() {
		
	}
	
	//Metodos
	@Override
	public GeneroDTO create(GeneroDTO generoDTO) {
		Genero gene = new Genero(
				generoDTO.getNombre()
				);
		this.generoRepository.save(gene);
		return new GeneroDTO(gene);
	}
	
	@Override
	public GeneroDTO findById(Long idGenero) {
		Genero genero = this.generoRepository.findById(idGenero).orElse(null);
		GeneroDTO generoDTO = null;
		if(genero != null) {
			generoDTO = new GeneroDTO(genero);
		}
		return generoDTO;
	}
	
	@Override
	public List<GeneroDTO> buscarNombre(String nombre) {
		List<Genero> generos = (List<Genero>) this.generoRepository.buscarNombre(nombre);
		List<GeneroDTO> generosDTO = new ArrayList<GeneroDTO>();
		generos.stream().forEach(name -> generosDTO.add(new GeneroDTO(name)));
		return generosDTO;
	}
	
	@Override
	public List<GeneroDTO> findAll() {
		List<Genero> generos = (List<Genero>) this.generoRepository.findAll();
		List<GeneroDTO> generoDTO = new ArrayList<GeneroDTO>();
		generos.stream().forEach(gen -> generoDTO.add(new GeneroDTO(gen)));
		return generoDTO;
	}
	
	@Override
	public GeneroDTO update(GeneroDTO generoDTO) {
		Genero genero = this.generoRepository.findById(generoDTO.getId()).get();
			 genero.setNombre(generoDTO.getNombre());
			 genero = this.generoRepository.save(genero);
		return generoDTO;
	}
	
	@Override
	public void deleteById(Long id) {
		this.generoRepository.deleteById(id);
	}
}
