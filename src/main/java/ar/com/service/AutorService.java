package ar.com.service;

import java.util.List;

import ar.com.dto.AutorDTO;

/**
 * @author Carlos
 *
 */
public interface AutorService {
	
	public AutorDTO create(AutorDTO autorDTO);
	
	public AutorDTO findById(Long idAutor);
	
	public List<AutorDTO> findAll();

	public void deleteById(Long id);

	public AutorDTO update(AutorDTO autorDTO);

	public List<AutorDTO> buscarApellido(String apellido);

	public List<AutorDTO> buscarApeyNom(String apellido, String nombre);
}
