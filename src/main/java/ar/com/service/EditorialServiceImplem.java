/**
 * 
 */
package ar.com.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.com.dto.EditorialDTO;
import ar.com.model.Editorial;
import ar.com.repository.EditorialRepository;

/**
 * @author Carlos
 *
 */
@Service
public class EditorialServiceImplem implements EditorialService {
		
	//Atributos
	@Autowired
	private EditorialRepository editorialRepository;
	
	public EditorialServiceImplem() {
		
	}
	
	//Metodos
	@Override
	public EditorialDTO create(EditorialDTO editorialDTO) {
		Editorial editorial = new Editorial(
				editorialDTO.getNombre());
		this.editorialRepository.save(editorial);
		editorialDTO.setId(editorial.getId());
		return editorialDTO;
	}

	@Override 
	public EditorialDTO findById(Long idEditorial) {
		Editorial editor = this.editorialRepository.findById(idEditorial).orElse(null);
		EditorialDTO editorialDTO = null;
		if (editor != null) {
			editorialDTO = new EditorialDTO(editor);
		}
		return editorialDTO;
	}
	
	@Override
	public List<EditorialDTO> buscarEditorial(String nombre){
		List<Editorial> editoriales = this.editorialRepository.buscarEditorial(nombre);
		List<EditorialDTO> editorialDTO = new ArrayList<EditorialDTO>();
		editoriales.stream().forEach(ed -> editorialDTO.add(new EditorialDTO(ed)));
		return editorialDTO;
	}
	
	@Override
	public List<EditorialDTO> findAll(){
		List<Editorial> editoriales = (List<Editorial>) this.editorialRepository.findAll();
		List<EditorialDTO> editorialDTO = new ArrayList<EditorialDTO>();
		editoriales.stream().forEach(edt -> editorialDTO.add(new EditorialDTO(edt)));
		return editorialDTO;
	}

	@Override
	public void deleteById(Long id) {
		this.editorialRepository.deleteById(id);
	}

	@Override
	public EditorialDTO update(EditorialDTO editorialDTO) {
		Editorial editorial = this.editorialRepository.findById(editorialDTO.getId()).get();
			      editorial.getNombre();
		this.editorialRepository.save(editorial);
		return editorialDTO;
	}

}
