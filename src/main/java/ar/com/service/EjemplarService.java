/**
 * 
 */
package ar.com.service;

import java.util.List;

import ar.com.dto.EjemplarDTO;

/**
 * @author Carlos
 *
 */
public interface EjemplarService {

	public EjemplarDTO create(EjemplarDTO ejemplarDTO);
	
	public EjemplarDTO findById(Long idEjemplar);
	
	public List<EjemplarDTO> findAll();

	public List<EjemplarDTO> buscarDispon(Long idLibro);

	public EjemplarDTO update(EjemplarDTO ejemplarDTO);
}
