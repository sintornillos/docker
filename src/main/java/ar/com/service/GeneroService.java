package ar.com.service;

import java.util.List;

import ar.com.dto.GeneroDTO;

/**
 * @author Carlos 
 *
 */
public interface GeneroService {

	public GeneroDTO create(GeneroDTO generoDTO);
	
	public GeneroDTO findById(Long idGenero);
	
	public List<GeneroDTO> findAll();
	
	public GeneroDTO update(GeneroDTO generoDTO);

	public void deleteById(Long id);
	
	public List<GeneroDTO> buscarNombre(String nombre);
	
}
