package ar.com.model;
/**
 * @author Carlos
 *
 */
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity(name="Libro")
@Table(name="libro")
public class Libro {

	//Atributos
	@Id
	@Column (name="id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="nativoDeBaseDeDatos")
	@GenericGenerator(name="nativoDeBaseDeDatos", strategy="native")
	private Long id;
	
	@Column (name = "BibliotecaId")
	private Long bibliotecaId;
	
	@Column (name="Titulo")
	private String titulo;
	
	@Column (name="subTitulo")
	private String subTitulo;
	
	@Column (name="cdu")
	private String cdu;
	
	@ManyToOne
	@JoinColumn(name = "temaId", foreignKey=@ForeignKey(name="fk_idLibro_idTema"))
	private Tema tema;

	@ManyToOne
	@JoinColumn(name = "generoId", foreignKey=@ForeignKey(name="fk_idLibro_idGenero"))
	private Genero genero;
	
	@Column (name="coleccion")
	private String coleccion;
	
	@Column (name="descripcion")
	private String descripcion;
	
	@ManyToMany
	private List<Autor> autores = new ArrayList<Autor>();

	@OneToMany(mappedBy = "libro", targetEntity = Ejemplar.class, cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Ejemplar> ejemplares;
	
	//Constructores
	public Libro() {
		super();
	}

	public Libro(String titulo, String subTitulo, List<Autor> autores) {
		super();
		this.titulo = titulo;
		this.subTitulo = subTitulo;
		this.autores = new ArrayList<>();
	}

	public  Libro(long biblioId, String titulo, String subTitulo, String cdu, Tema tema, Genero genero, String coleccion, String descripcion) {
		this.bibliotecaId = biblioId;
		this.titulo = titulo;
		this.subTitulo = subTitulo;
		this.cdu = cdu;
		this.tema = tema;
		this.genero = genero;
		this.coleccion = coleccion;
		this.descripcion = descripcion;
	}
	
//	public  Libro(String titulo, String subTitulo, String cdu, Tema tema, Genero genero, String coleccion, String descripcion, List<Ejemplar> ejemplares) {
//		this.titulo = titulo;
//		this.subTitulo = subTitulo;
//		this.cdu = cdu;
//		this.tema = tema;
//		this.genero = genero;
//		this.coleccion = coleccion;
//		this.descripcion = descripcion;
//		this.ejemplares = new ArrayList<>();
//	}
//	
	public  Libro(long biblioId, String titulo, String subTitulo, String cdu, Tema tema, Genero genero, String coleccion, String descripcion, List<Autor> autores, List<Ejemplar> ejemplares) {
		this.bibliotecaId = biblioId;
		this.titulo = titulo;
		this.subTitulo = subTitulo;
		this.cdu = cdu;
		this.tema = tema;
		this.genero = genero;
		this.coleccion = coleccion;
		this.descripcion = descripcion;
		this.autores = new ArrayList<>();
		this.ejemplares = new ArrayList<>();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Libro))
			return false;
		Libro other = (Libro) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	//Get & Sets
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getSubTitulo() {
		return subTitulo;
	}

	public void setSubTitulo(String subTitulo) {
		this.subTitulo = subTitulo;
	}

	public List<Autor> getAutores() {
		return autores;
	}

	public void setAutores(List<Autor> autores) {
		this.autores = autores;
	}

	public List<Ejemplar> getEjemplares() {
		return ejemplares;
	}

	public void setEjemplares(List<Ejemplar> ejemplares) {
		this.ejemplares = ejemplares;
	}

	public String getCdu() {
		return cdu;
	}

	public void setCdu(String cdu) {
		this.cdu = cdu;
	}

	public Tema getTema() {
		return tema;
	}

	public void setTema(Tema tema) {
		this.tema = tema;
	}

	public Genero getGenero() {
		return genero;
	}

	public void setGenero(Genero genero) {
		this.genero = genero;
	}

	public String getColeccion() {
		return coleccion;
	}

	public void setColeccion(String coleccion) {
		this.coleccion = coleccion;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public void addAutor(Autor autor) {
		this.autores.add(autor);
	}

	public Long getBibliotecaId() {
		return bibliotecaId;
	}

	public void setBibliotecaId(Long bibliotecaId) {
		this.bibliotecaId = bibliotecaId;
	}
	
	
}
