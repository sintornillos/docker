package ar.com.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;


@Entity(name="Socio")
@Table(name="socio")
public class Socio {

	// atributos
	@Id
	@Column(name="idSocio")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="nativoDeBaseDeDatos")
	@GenericGenerator(name="nativoDeBaseDeDatos", strategy="native")
	private Long id;
	
	@Column(name="Apellido")
	private String apellido;

	@Column(name="Nombre")
	private String nombre;

	@Column(name="NroSocio")
	private String nroSocio;
	
	@Column(name="NroDocum")
	private Long nroDocumento;
	
	@Column(name="Direcc")
	private String direcc;

	@Column(name="Barrio")
	private String barrio;
	
	@Column(name="Ciudad")
	private String ciudad;

	@Column(name="Telefono")
	private String telefono;

	@Column(name="Celular")
	private String celular;
	
	@Column(name="Email")
	private String email;
	
	@Column(name="CPostal")
	private Long cpostal;
	
	@Column(name="FecAlta")
	private LocalDate fecAlta;
	
	@Column(name="Sexo")
	private String sexo;
	
	@Column(name="categoria")
	private String categoria;
	
	
	// constructores
	public Socio() {
		super();
	}
	
	public Socio(String apellido, String nombre, String nroSoc, Long nroDocum, String direcc, String barrio, String ciudad, 
			String telefono, String celular, String email, Long cp, String sexo, String categ) {
		this();
	    this.apellido = apellido;
	    this.nombre = nombre;
	    this.nroSocio = nroSoc;
	    this.nroDocumento = nroDocum;
	    this.direcc = direcc;
	    this.barrio = barrio;
	    this.ciudad = ciudad;
	    this.telefono = telefono;
	    this.celular = celular;
	    this.email = email;
	    this.cpostal = cp;
	    this.sexo = sexo;
	    this.categoria = categ;
	    
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Socio))
			return false;
		Socio other = (Socio) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getNroSocio() {
		return nroSocio;
	}

	public void setNroSocio(String nroSoc) {
		this.nroSocio = nroSoc;
	}

	public Long getNroDocumento() {
		return nroDocumento;
	}

	public void setNroDocumento(Long nroDocumento) {
		this.nroDocumento = nroDocumento;
	}

	public String getDirecc() {
		return direcc;
	}

	public void setDirecc(String direcc) {
		this.direcc = direcc;
	}

	public String getBarrio() {
		return barrio;
	}

	public void setBarrio(String barrio) {
		this.barrio = barrio;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Long getCpostal() {
		return cpostal;
	}

	public void setCpostal(Long cpostal) {
		this.cpostal = cpostal;
	}

	public LocalDate getFecAlta() {
		return fecAlta;
	}

	public void setFecAlta(LocalDate fecAlta) {
		this.fecAlta = fecAlta;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	
}	