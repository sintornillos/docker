package ar.com.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.Where;

/**
 * @author Carlos
 *
 */
/*  Datos del ejemplar
ISBN - Nro.Inventario - Signatura Topografica - Editorial 
Lugar Edición - Fecha Edición -  
*/

@Entity(name="Ejemplar")
@Table(name="ejemplar")
@SQLDelete(sql = "UPDATE Ejemplar SET state = 'DELETED' WHERE id = ?")
@Where(clause = "state is null")
public class Ejemplar {

	//Atributos
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="nativoDeBaseDeDatos")
	@GenericGenerator(name="nativoDeBaseDeDatos", strategy="native")
	private long id;
	
	@Column(name="ISBN", length=25, nullable=false)
	@Type(type="long")
	private long isbn;
	
	@Column(name="Inventario")
	private String inventario;
	
	@Column(name="SigTopog")
	private String sigTopografica;
	
	@Column(name="Lugar")
	private String lugar;
	
	@Column(name="Fecha")
	private LocalDate fecha;
	
	@ManyToOne
	@JoinColumn(name = "editorialId", foreignKey=@ForeignKey(name="fk_idEditorial_idEjemplar"))
	private Editorial editorial;

	@ManyToOne
	@JoinColumn(name="libroId", foreignKey=@ForeignKey(name="fk_idEjemplar_idLibro"))
	private Libro libro;
	
	@ManyToOne
	@JoinColumn(name="estadoId", foreignKey=@ForeignKey(name="fk_idEjemplar_idEstado"))
	private EstadoEjemplar estado;
	
	@Column(name = "State")
	private String state;
	

	//Constructores
	public Ejemplar() {
		super();
	}
	
	public Ejemplar(long isbn, String invent, String sigTopog, String lugar, LocalDate fEdicion, Editorial editorial, EstadoEjemplar estado, Libro libro) {
		this();
		this.isbn = isbn;
		this.inventario = invent;
		this.sigTopografica = sigTopog;
		this.lugar = lugar;
		this.fecha = fEdicion;
		this.editorial = editorial;
		this.estado = estado;
		this.libro = libro;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Ejemplar))
			return false;
		Ejemplar other = (Ejemplar) obj;
		if (id != other.id)
			return false;
		return true;
	}

	//Get & Sets

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getIsbn() {
		return isbn;
	}

	public void setIsbn(long isbn) {
		this.isbn = isbn;
	}

	public String getInventario() {
		return inventario;
	}

	public void setInventario(String inventario) {
		this.inventario = inventario;
	}

	public String getSigTopografica() {
		return sigTopografica;
	}

	public void setSigTopografica(String sigTopografica) {
		this.sigTopografica = sigTopografica;
	}

	public Editorial getEditorial() {
		return editorial;
	}

	public void setEditorial(Editorial editorial) {
		this.editorial = editorial;
	}

	public String getLugar() {
		return lugar;
	}

	public void setLugar(String lugar) {
		this.lugar = lugar;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public Libro getLibro() {
		return libro;
	}

	public void setLibro(Libro libro) {
		this.libro = libro;
	}

	public EstadoEjemplar getEstado() {
		return estado;
	}

	public void setEstado(EstadoEjemplar estado) {
		this.estado = estado;
	}
	
}
