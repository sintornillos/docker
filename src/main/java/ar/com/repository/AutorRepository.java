package ar.com.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ar.com.model.Autor;

/**
 * @author Carlos
 *
 */

@Repository
public interface AutorRepository extends CrudRepository<Autor, Long>, AutorRepositoryCustom, AutorRepositoryCustomClass {


}
