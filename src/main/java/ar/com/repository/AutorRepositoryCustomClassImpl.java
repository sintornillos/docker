package ar.com.repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.hibernate.Session;

import ar.com.model.Autor;

public class AutorRepositoryCustomClassImpl implements AutorRepositoryCustomClass {

	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	@Transactional
	public List<Autor> buscarApellidoNombre(String apellido, String nombre){
		Session session = this.entityManager.unwrap(Session.class);
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<Autor> query = builder.createQuery(Autor.class);
		Root<Autor> root = query.from(Autor.class);
		
		List<Predicate> predicates = new ArrayList<Predicate>();
		if ( nombre != null) {
			predicates.add(builder.like(root.<String>get("nombre"), "%"+ nombre + "%"));
			
		}
		if ( apellido != null) {
			predicates.add(builder.like(root.<String>get("apellido"), "%"+ apellido + "%"));
			
		}
		query.where(builder.and(predicates.toArray(new Predicate[] {})));
		
		List<Autor> autores = session.createQuery(query).getResultList();
		return autores;
		
	}
}
