package ar.com.repository;

import org.springframework.data.repository.CrudRepository;

import ar.com.model.Prestamo;

public interface PrestamoRepository extends CrudRepository<Prestamo, Long>, PrestamoRepositoryCustom{

}
