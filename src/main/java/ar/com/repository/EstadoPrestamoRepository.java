/**
 * 
 */
package ar.com.repository;

import org.springframework.data.repository.CrudRepository;

import ar.com.model.EstadoPrestamo;

/**
 * @author Carlos
 *
 */
public interface EstadoPrestamoRepository extends CrudRepository<EstadoPrestamo, Long> {

}
