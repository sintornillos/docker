package ar.com.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ar.com.model.Socio;

@Repository
public interface SocioRepository extends CrudRepository<Socio, Long>, SocioRepositoryCustom, SocioRepositoryCustomClass {

}
