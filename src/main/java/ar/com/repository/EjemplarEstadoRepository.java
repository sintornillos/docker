/**
 * 
 */
package ar.com.repository;

import org.springframework.data.repository.CrudRepository;

import ar.com.model.EstadoEjemplar;

/**
 * @author Carlos
 *
 */
public interface EjemplarEstadoRepository extends CrudRepository<EstadoEjemplar, Long> {

}
