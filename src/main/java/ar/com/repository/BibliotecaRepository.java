/**
 * 
 */
package ar.com.repository;

import org.springframework.data.repository.CrudRepository;

import ar.com.model.Biblioteca;

/**
 * @author Carlos
 *
 */
public interface BibliotecaRepository extends CrudRepository<Biblioteca, Long> {

}
