package ar.com.repository;

import java.util.List;

import ar.com.model.Autor;

public interface AutorRepositoryCustomClass {
	
	public List<Autor> buscarApellidoNombre(String apellido, String nombre);

}
