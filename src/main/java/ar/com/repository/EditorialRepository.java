/**
 * 
 */
package ar.com.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ar.com.model.Editorial;

/**
 * @author Carlos
 *
 */

@Repository
public interface EditorialRepository extends CrudRepository<Editorial, Long>, EditorialRepositoryCustom, EditorialRepositoryCustomClass {

}
