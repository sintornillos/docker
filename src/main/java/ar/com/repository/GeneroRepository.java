package ar.com.repository;

import org.springframework.data.repository.CrudRepository;

import ar.com.model.Genero;

public interface GeneroRepository extends CrudRepository<Genero, Long>, GeneroRepositoryCustom {

}
