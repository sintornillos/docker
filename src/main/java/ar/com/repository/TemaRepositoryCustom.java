package ar.com.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import ar.com.model.Tema;

public interface TemaRepositoryCustom {
	
	@Query("SELECT t FROM Tema t WHERE t.nombre like %:nombre% ")
	public List<Tema> buscarTema(String nombre);
	

}
