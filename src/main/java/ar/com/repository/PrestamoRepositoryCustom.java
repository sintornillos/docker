package ar.com.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import ar.com.model.Prestamo;
import ar.com.model.Socio;

public interface PrestamoRepositoryCustom {
	
	@Query("SELECT p FROM Prestamo p WHERE p.socio.id = :socioId ")
	public List<Prestamo> buscarPrestamo(Socio socioId);

}
