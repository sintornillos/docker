package ar.com.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import ar.com.model.Autor;

public interface AutorRepositoryCustom {
	
	@Query("select a from Autor a where a.apellido like %:apellido% ")
	List<Autor> buscarApellido(String apellido);

}
