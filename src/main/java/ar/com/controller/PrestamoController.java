package ar.com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ar.com.dto.PrestamoDTO;
import ar.com.service.PrestamoService;

@RestController
@RequestMapping("/prestamo")
@CrossOrigin(origins = "*", maxAge = 3600, methods = {RequestMethod.POST, RequestMethod.GET, RequestMethod.DELETE, RequestMethod.PUT, RequestMethod.OPTIONS})
public class PrestamoController {
	
	//Atributos
		@Autowired
		private PrestamoService prestamoService;
		
		@RequestMapping(method=RequestMethod.POST)
		public ResponseEntity<PrestamoDTO> create(@RequestBody PrestamoDTO prestamoDTO){
			prestamoDTO = this.prestamoService.create(prestamoDTO);
			return new ResponseEntity<PrestamoDTO>(prestamoDTO, HttpStatus.CREATED);
		}
		
		@RequestMapping(value="/findAll", method=RequestMethod.GET)
		public ResponseEntity<List<PrestamoDTO>> findAll() throws NotFoundException{
			List<PrestamoDTO> prestamoDTO = this.prestamoService.findAll();
			if(prestamoDTO == null) {
				throw new NotFoundException();
			}
			return new ResponseEntity<List<PrestamoDTO>> (prestamoDTO, HttpStatus.OK);
		}
		
		@RequestMapping(value="/findById/{id}", method=RequestMethod.GET)
		public ResponseEntity<PrestamoDTO> findById(@PathVariable("id") long id) throws NotFoundException {
			PrestamoDTO prestamoDTO = this.prestamoService.findById(id);
			if(prestamoDTO == null) {
				throw new NotFoundException();
			}
			return new ResponseEntity<PrestamoDTO> (prestamoDTO, HttpStatus.OK);
		}
		
		@RequestMapping(value="/devolver" , method=RequestMethod.PUT)
		public ResponseEntity<PrestamoDTO> devolver(@RequestBody PrestamoDTO prestamoDTO) throws NotFoundException{
			prestamoDTO = this.prestamoService.devolver(prestamoDTO);
			return new ResponseEntity<PrestamoDTO>(prestamoDTO, HttpStatus.OK);
		}
		
		@RequestMapping(value="/prestar", method=RequestMethod.PUT)
		public ResponseEntity<PrestamoDTO> prestar(@RequestBody PrestamoDTO prestamoDTO) throws javassist.NotFoundException{
			prestamoDTO = this.prestamoService.prestar(prestamoDTO);
			return new ResponseEntity<PrestamoDTO>(prestamoDTO, HttpStatus.OK);
		}
		
		@RequestMapping(value="/renovar", method=RequestMethod.PUT)
		public ResponseEntity<PrestamoDTO> renovar(@RequestBody PrestamoDTO prestamoDTO) throws javassist.NotFoundException{
			prestamoDTO = this.prestamoService.renovar(prestamoDTO);
			return new ResponseEntity<PrestamoDTO>(prestamoDTO, HttpStatus.OK);
		}
		
		@ExceptionHandler(NotFoundException.class)
		public ResponseEntity<Void> exceptionHandler(Exception excep) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
		
}
