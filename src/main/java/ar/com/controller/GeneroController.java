package ar.com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ar.com.dto.GeneroDTO;
import ar.com.service.GeneroService;


/**
 * @author Carlos
 *
 */
@RestController
@RequestMapping("/genero")
@CrossOrigin(origins = "*", maxAge = 3600, methods = {RequestMethod.POST, RequestMethod.GET, RequestMethod.DELETE, RequestMethod.PUT, RequestMethod.OPTIONS})
public class GeneroController {
	
	//Atributos
	@Autowired
	private GeneroService generoService;
	
	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<GeneroDTO> create(@RequestBody GeneroDTO generoDTO) {
		generoDTO = this.generoService.create(generoDTO);
		return new ResponseEntity<GeneroDTO>(generoDTO, HttpStatus.CREATED);
	}
	
	@RequestMapping(value="/findAll", method=RequestMethod.GET)
	public ResponseEntity<List<GeneroDTO>> findAll() throws NotFoundException{
		List<GeneroDTO> generoDTO = this.generoService.findAll();
		if(generoDTO == null) {
			throw new NotFoundException();
		}
		return new ResponseEntity<List<GeneroDTO>>(generoDTO, HttpStatus.OK);
	}
	@RequestMapping(value="/findById/{id}", method=RequestMethod.GET)
	public ResponseEntity<GeneroDTO> findById(@PathVariable("id") long id) throws NotFoundException {
		GeneroDTO generoDTO = this.generoService.findById(id);
		if(generoDTO == null) {
			throw new NotFoundException();
		}
		return new ResponseEntity<GeneroDTO> (generoDTO, HttpStatus.OK);
	}
	
	@RequestMapping(value="/findByName/{name}", method=RequestMethod.GET)
	public ResponseEntity<List<GeneroDTO>> findByName(@PathVariable("name") String name) throws NotFoundException {
		List<GeneroDTO> generoDTO = this.generoService.buscarNombre(name);
		if(generoDTO == null) {
			throw new NotFoundException();
		}
		return new ResponseEntity<List<GeneroDTO>>(generoDTO, HttpStatus.OK);
	}
	
	@RequestMapping(value="/delete/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<Void> delete(@PathVariable("id") long id) throws NotFoundException {
	    this.generoService.deleteById(id);
		return new ResponseEntity<Void>(HttpStatus.ACCEPTED);
	}
	
	@ExceptionHandler(NotFoundException.class)
	public ResponseEntity<Void> exceptionHandler(Exception excep) {
		return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	}
	
}
