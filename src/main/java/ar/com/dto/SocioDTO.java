package ar.com.dto;

import java.time.LocalDate;

import ar.com.model.Socio;

public class SocioDTO {

	private Long id;
	private String apellido;
	private String nombre;
	private String nroSocio;
	private Long nroDocumento;
	private String direcc;
	private String barrio;
	private String ciudad;
	private String telefono;
	private String celular;
	private String email;
	private Long cpostal;
	private LocalDate fecAlta;
	private String sexo;
	private String categoria;
	
	// constructores
	public SocioDTO() {
		super();
	}

	public SocioDTO(String apellido, String nombre, String nroSoc, Long doc, String direcc, String barrio, String ciudad, 
			Long cp, String telefono, String celular, LocalDate fAlta, String email, String sexo, String categoria ) {
		super();
		this.apellido = apellido;
		this.nombre = nombre;
		this.nroSocio = nroSoc;
		this.nroDocumento = doc;
		this.direcc = direcc;
		this.barrio = barrio;
		this.ciudad = ciudad;
		this.cpostal = cp;
		this.telefono = telefono;
		this.celular = celular;
		this.fecAlta = fAlta;
		this.email = email;
		this.sexo = sexo;
		this.categoria = categoria;
		
	}
	
	public SocioDTO(Socio socio) {
		this(socio.getApellido(),
			 socio.getNombre(),
			 socio.getNroSocio(),
			 socio.getNroDocumento(),
			 socio.getDirecc(),
			 socio.getBarrio(),
			 socio.getCiudad(),
			 socio.getCpostal(),
			 socio.getTelefono(),
			 socio.getCelular(),
			 socio.getFecAlta(),
			 socio.getEmail(),
			 socio.getSexo(),
			 socio.getCategoria());
		this.id = socio.getId();
	}
	

	// get y set
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNroSocio() {
		return nroSocio;
	}

	public void setNroSocio(String nroSoc) {
		this.nroSocio = nroSoc;
	}
	
	public Long getNroDocumento() {
		return nroDocumento;
	}

	public void setNroDocumento(Long nroDocumento) {
		this.nroDocumento = nroDocumento;
	}

	public String getDirecc() {
		return direcc;
	}

	public void setDirecc(String direcc) {
		this.direcc = direcc;
	}

	public String getBarrio() {
		return barrio;
	}

	public void setBarrio(String barrio) {
		this.barrio = barrio;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Long getCpostal() {
		return cpostal;
	}

	public void setCpostal(Long cpostal) {
		this.cpostal = cpostal;
	}

	public LocalDate getFecAlta() {
		return fecAlta;
	}

	public void setFecAlta(LocalDate fecAlta) {
		this.fecAlta = fecAlta;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

}
