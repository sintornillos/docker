package ar.com.dto;

import ar.com.model.EstadoPrestamo;

public class EstadoPrestamoDTO {
	
	//Atributos
	private long id;
	private String tipoEstado;
	
	//Constructores
	public EstadoPrestamoDTO() {
		super();
	}
	
	public EstadoPrestamoDTO(String estado) {
		super();
		this.tipoEstado = estado;
	}
	
	public EstadoPrestamoDTO(EstadoPrestamo estado) {
		this(estado.getDescripcion());
		this.id = estado.getId();
	}

	//Gets & Sets
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTipoEstado() {
		return tipoEstado;
	}

	public void setTipoEstado(String tipoEstado) {
		this.tipoEstado = tipoEstado;
	}

	
}
