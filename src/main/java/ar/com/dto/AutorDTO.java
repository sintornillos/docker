package ar.com.dto;

import ar.com.model.Autor;

public class AutorDTO {
	
	private Long id;
	private String apellido;
	private String nombre;
	private String nacionalidad;
	
	// constructores
	public AutorDTO() {	}
	
	public AutorDTO(String apellido, String nombre, String nacionalidad) {
		super();
		this.apellido = apellido;
		this.nombre = nombre;
		this.nacionalidad = nacionalidad;
	}

	public AutorDTO(Autor autor) {
		this(autor.getApellido(),
			 autor.getNombre(),
			 autor.getNacionalidad());
		this.id = autor.getId();
	}

	// get y set
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNacionalidad() {
		return nacionalidad;
	}

	public void setNacionalidad(String nacionalidad) {
		this.nacionalidad = nacionalidad;
	}

}
