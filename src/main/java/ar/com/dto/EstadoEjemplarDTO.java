package ar.com.dto;

import ar.com.model.EstadoEjemplar;

public class EstadoEjemplarDTO {
	
	//Atributos
	private long id;
	private String tipoEstado;
	
	//Constructores
	public EstadoEjemplarDTO() {
		super();
	}
	
	public EstadoEjemplarDTO(String estado) {
		super();
		this.tipoEstado = estado;
	}
	
	public EstadoEjemplarDTO(EstadoEjemplar estado) {
		this(estado.getDescripcion());
		this.id = estado.getId();
	}

	//Get & Set
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTipoEstado() {
		return tipoEstado;
	}

	public void setTipoEstado(String tipoEstado) {
		this.tipoEstado = tipoEstado;
	}

	
}
