package ar.com.dto;

import ar.com.model.Genero;

public class GeneroDTO {

	private Long id;
	private String nombre;
	
	//Constructores
	public GeneroDTO() {
		super();
	}
	
	public GeneroDTO(String nombre) {
		super();
		this.nombre = nombre;
	}
	
	public GeneroDTO(Genero nombre) {
		this(nombre.getNombre());
		this.id = nombre.getId();
	}
	
	//Get y Set
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	

}
