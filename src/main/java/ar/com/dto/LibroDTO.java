package ar.com.dto;

import java.util.ArrayList;
import java.util.List;

import ar.com.model.Autor;
import ar.com.model.Ejemplar;
import ar.com.model.Libro;

public class LibroDTO {

	//Atributos
	private long id;
	private long bibliotecaId;
	private String titulo;
	private String subTitulo;
	private String cdu;
	private List<Long> autoresId;
	private long generoId;
	private long temaId;
	private List<AutorDTO> autoresDto;
	private List<EjemplarDTO> ejemplaresDto;
	private String coleccion;
	private String descripcion;
	private int ctdadEjemplaresDisponibles;
	
	//Constructores
	public LibroDTO() {	}
//	public LibroDTO(String titulo, String subTitulo, Autor autor, Ejemplar ejemplar) {
	public LibroDTO(String titulo, String subTitulo) {
		super();
		this.titulo = titulo;
		this.subTitulo = subTitulo;
	}
	
	public LibroDTO(String titulo, String subTitulo, List<Autor> autores, List<Ejemplar> ejemplares) {
		this.titulo = titulo;
		this.subTitulo = subTitulo;
		this.autoresDto = new ArrayList<AutorDTO>();
		autores.stream().forEach( autor -> this.autoresDto.add(new AutorDTO(autor)));
		this.ejemplaresDto = new ArrayList<EjemplarDTO>();
		ejemplares.stream().forEach( ejemplar -> this.ejemplaresDto.add(new EjemplarDTO(ejemplar)));
	}
	
	public LibroDTO(long biblioId, String titulo, String subTitulo, String cdu, String coleccion, String descripcion, 
			 long id, long idGenero, List<Autor> autores, List<Ejemplar> ejemplares) {
		this.bibliotecaId = biblioId;
		this.titulo = titulo;
		this.subTitulo = subTitulo;
		this.cdu = cdu;
		this.coleccion = coleccion;
		this.descripcion = descripcion;
		this.temaId = id;
		this.generoId = idGenero;
		this.autoresDto = new ArrayList<AutorDTO>();
		autores.stream().forEach(aut -> this.autoresDto.add(new AutorDTO(aut)));
		this.ejemplaresDto = new ArrayList<EjemplarDTO>();
		ejemplares.stream().forEach( ejemplar -> this.ejemplaresDto.add(new EjemplarDTO(ejemplar)));
	}
	
	public LibroDTO(Libro libro) {
		this(libro.getBibliotecaId(),
			 libro.getTitulo(),
			 libro.getSubTitulo(),
			 libro.getCdu(),
			 libro.getColeccion(),
			 libro.getDescripcion(),
			 libro.getTema().getId(),
			 libro.getGenero().getId(),
			 libro.getAutores(),
			 libro.getEjemplares());
		this.id = libro.getId();
	}

	//Get & Sets
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	
	public long getBibliotecaId() {
		return bibliotecaId;
	}

	public void setBibliotecaId(long bibliotecaId) {
		this.bibliotecaId = bibliotecaId;
	}

	public List<AutorDTO> getAutoresDto() {
		return autoresDto;
	}

	public void setAutoresDto(List<AutorDTO> autoresDto) {
		this.autoresDto = autoresDto;
	}

	public List<EjemplarDTO> getEjemplaresDto() {
		return ejemplaresDto;
	}

	public void setEjemplaresDto(List<EjemplarDTO> ejemplaresDto) {
		this.ejemplaresDto = ejemplaresDto;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getSubTitulo() {
		return subTitulo;
	}

	public void setSubTitulo(String subTitulo) {
		this.subTitulo = subTitulo;
	}

	public List<AutorDTO> getAutoresDTO() {
		return autoresDto;
	}

	public void setAutoresDTO(List<AutorDTO> autores) {
		this.autoresDto = autores;
	}

	public List<EjemplarDTO> getEjemplares() {
		return ejemplaresDto;
	}

	public void setEjemplares(List<EjemplarDTO> ejemplares) {
		this.ejemplaresDto = ejemplares;
	}

	public String getCdu() {
		return cdu;
	}

	public void setCdu(String cdu) {
		this.cdu = cdu;
	}

	public List<Long> getAutoresId() {
		return autoresId;
	}

	public void setAutoresId(List<Long> autoresId) {
		this.autoresId = autoresId;
	}

	public long getGeneroId() {
		return generoId;
	}

	public void setGeneroId(long generoId) {
		this.generoId = generoId;
	}

	public long getTemaId() {
		return temaId;
	}

	public void setTemaId(long temaId) {
		this.temaId = temaId;
	}

	public String getColeccion() {
		return coleccion;
	}

	public void setColeccion(String coleccion) {
		this.coleccion = coleccion;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	public int getCtdadEjemplaresDisponibles() {
		return ctdadEjemplaresDisponibles;
	}
	
	public void setCtdadEjemplaresDisponibles(int ctdadEjemplaresDisponibles) {
		this.ctdadEjemplaresDisponibles = ctdadEjemplaresDisponibles;
	}

	
}
