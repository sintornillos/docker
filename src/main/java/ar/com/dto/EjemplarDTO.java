package ar.com.dto;

import java.time.LocalDate;

import ar.com.model.Ejemplar;

/**
 * @author Carlos
 *
 */
public class EjemplarDTO {
	
	//Atributos
	private Long id;
	private Long isbn;
	private String inventario;
	private String sigTopografica;
	private String lugar;
	private LocalDate fecha;
	private Long editorialId;
	private Long libroId;
	private Long estadoId;

	//Constructores
	public EjemplarDTO() {
		super();
	}
	
	public EjemplarDTO(Ejemplar ejemplar) {
		this(ejemplar.getIsbn(),
			 ejemplar.getInventario(),
			 ejemplar.getSigTopografica(),
			 ejemplar.getLugar(),
			 ejemplar.getFecha(),
			 ejemplar.getEditorial().getId(),
			 ejemplar.getLibro().getId(),
			 ejemplar.getEstado().getId());
		this.id = ejemplar.getId();
		
	}

	public EjemplarDTO(Long isbn, String inventario, String sigTopografica, String lugar, LocalDate fecha) {
		this.isbn = isbn;
		this.inventario = inventario;
		this.sigTopografica = sigTopografica;
		this.lugar = lugar;
		this.fecha = fecha;
	}
	
	public EjemplarDTO(Long isbn, String inventario, String sigTopografica, String lugar, LocalDate fecha,
			Long editorialId, Long libroId, Long estadoId) {
		this.isbn = isbn;
		this.inventario = inventario;
		this.sigTopografica = sigTopografica;
		this.lugar = lugar;
		this.fecha = fecha;
		this.editorialId = editorialId;
		this.libroId = libroId;
		this.estadoId = estadoId;
	}

	//Gets & Sets
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIsbn() {
		return isbn;
	}

	public void setIsbn(Long isbn) {
		this.isbn = isbn;
	}

	public String getInventario() {
		return inventario;
	}

	public void setInventario(String inventario) {
		this.inventario = inventario;
	}

	public String getSigTopografica() {
		return sigTopografica;
	}

	public void setSigTopografica(String sigTopografica) {
		this.sigTopografica = sigTopografica;
	}

	public String getLugar() {
		return lugar;
	}

	public void setLugar(String lugar) {
		this.lugar = lugar;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public Long getEditorialId() {
		return editorialId;
	}

	public void setEditorialId(Long editorialId) {
		this.editorialId = editorialId;
	}

	public Long getLibroId() {
		return libroId;
	}

	public void setLibroId(Long libroId) {
		this.libroId = libroId;
	}

	public Long getEstadoId() {
		return estadoId;
	}

	public void setEstadoId(Long estadoId) {
		this.estadoId = estadoId;
	}
	
}
