package ar.com.dto;

import ar.com.model.Tema;

public class TemaDTO {

	private Long id;
	private String nombre;
	
	//Constructores
	public TemaDTO() {
		super();
	}
	
	public TemaDTO(String nombre) {
		super();
		this.nombre = nombre;
	}
	
	public TemaDTO(Tema tema) {
		this(tema.getNombre());
		this.id = tema.getId();
	}
	
	//Get y Set
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

}
